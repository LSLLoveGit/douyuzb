//
//  NSDate-Extension.swift
//  DYZB
//
//  Created by LSL on 2022/1/24.
//

import Foundation

extension Date {
    static func getCurrentTime() -> String {
        let nowDate = NSDate()
        let interval = Int(nowDate.timeIntervalSince1970)
        return "\(interval)"
    }
}
