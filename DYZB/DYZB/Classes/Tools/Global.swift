//
//  Global.swift
//  DYZB
//
//  Created by LSL on 2022/1/16.
//
import Foundation
import UIKit

let _gScreenWidth = UIScreen.main.bounds.width
let _gScreenHeight = UIScreen.main.bounds.height
let _gStatusBarH = safeAreaInsets().top > 0 ? safeAreaInsets().top : 20.0
let _gTabBarMargin = safeAreaInsets().bottom > 0 ? safeAreaInsets().bottom : 0.0
let _gTabBarHeight = 49.0
let _gNavigationHeight = 44.0

func safeAreaInsets() -> (top: CGFloat, bottom: CGFloat) {
    if #available(iOS 11.0, *) {
        let inset = UIApplication.shared.delegate?.window??.safeAreaInsets
        return (inset?.top ?? 0, inset?.bottom ?? 0)
    } else {
        return (0, 0)
    }
}
