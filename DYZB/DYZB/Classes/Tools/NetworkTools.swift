//
//  NetworkTools.swift
//  DYZB
//
//  Created by LSL on 2022/1/23.
//

import UIKit
import Alamofire

import Moya
import SwiftyJSON
import ObjectMapper


enum MethodType {
    case GET
    case POST
}

//class NetworkTools {
//    class func requestData(type: HTTPMethod, URLString : String, parameters : [String : NSString]? = nil, finishedCallback: (_ result : AnyObject) -> ()) {
////        let method = type == .GET ? HTTPMethod.get : HTTPMethod.post
//
//        var encoding:ParameterEncoding = URLEncoding.default
//        if type == .post {
//            encoding = JSONEncoding.default
//        }
//
////        AF.request(URLString, method: type, parameters: parameters, encoder: JSONParameterEncoder.default, headers: nil).responseJSON { data in
////            finishedCallback(data.result)
////        }
//    }
//
//
//}
//


struct NetworkTools {
    //设置请求超时时间
    static let requestTimeoutClosure = { (endpoint: Endpoint, done: @escaping MoyaProvider<ChannelApi>.RequestResultClosure) in
        do {
            var request = try endpoint.urlRequest()
            request.timeoutInterval = 30
            done(.success(request))
        } catch {
            return
        }
    }
    static let provider = MoyaProvider<ChannelApi>(requestClosure: requestTimeoutClosure)
    
    static func request(
        _ target: ChannelApi,
        success successCallback: @escaping (JSON) -> Void,
        error errorCallback: @escaping (Int, String) -> Void,
        failure failureCallback: @escaping (MoyaError) -> Void
    ) {
        var needHud = true
//        switch target {
//        case .testConnect,.mainUrlAuth,.login,.getLatestVersion,.requestRefreshToken,.getIdpConfig:
//            needHud = false
//        default:
//            needHud = true
//        }
        if(needHud){
//            MCToast.mc_loading(text: "加载中...", duration: 0, respond: .default, callback: nil)
        }
        provider.request(target) { result in
            switch result {
            case let .success(response):
                do {
                    //如果数据返回成功则直接将结果转为JSON
                    try response.filterSuccessfulStatusCodes()
                    if(response.data.count == 0){
                        successCallback("success")
                    }else{
                        let json = try JSON(response.mapJSON())
                        successCallback(json)
                    }
                    if(needHud){
//                        MCToast.mc_remove()
                    }
                }
                catch let error {
                    let errorData = (error as! MoyaError).response?.data
                    print("errorData == ", errorData!)
                    let errorString = String(data: errorData!, encoding: .utf8) ?? "发生错误"
                    print("errorString == ", errorString)
//                    let errorInfo = Mapper<ErrorModel>().map(JSONString: errorString )
                    //如果数据获取失败，则返回错误状态码 与 错误信息
//                    errorCallback((error as! MoyaError).response?.statusCode ?? 0, (errorInfo?.error_description ?? errorString)!)
                    errorCallback((error as! MoyaError).response?.statusCode ?? 0, errorString)
                    if(needHud){
//                        MCToast.mc_remove()
                    }
//                    switch target {
//                    case .getServiceLatestVersion,.mainUrlAuth,.getLocationConfig:
//                        break;
//                    default:
//                        if(errorString.contains("请在绑定的设备上登录")){
//                            return
//                        }
//                        MCToast.mc_text((errorInfo?.error_description ?? errorString))
//                    }
                }
            case let .failure(error):
                failureCallback(error)
                if(needHud){
//                    MCToast.mc_remove()
                }
//                switch target {
//                case .getServiceLatestVersion,.mainUrlAuth,.getLocationConfig:
//                    break;
//                default:
//                    MCToast.mc_text(error.errorDescription ?? "发生错误")
//                }
            }
        }
    }
    
    static func requestWithProgress(
        _ target: ChannelApi,
        progress progressBack: @escaping (Double)->Void,
        success successCallback: @escaping (Int) -> Void,
        error errorCallback: @escaping (Int, String) -> Void,
        failure failureCallback: @escaping (MoyaError) -> Void
    ) {
        provider.request(target,progress: { progress in
            progressBack(progress.progress)
        }) {result in
            switch result {
            case let .success(response):
                successCallback(response.statusCode)
            case let .failure(error):
                failureCallback(error)
            }
        }
    }
}
