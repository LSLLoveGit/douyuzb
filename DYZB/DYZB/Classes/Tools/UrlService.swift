//
//  UrlService.swift
//  DYZB
//
//  Created by LSL on 2022/2/7.
//

import UIKit
import Moya

class UrlService: NSObject {

}

//请求分类
public enum ChannelApi {
    case getbigDataRoom(time:String)
    case getCycleDta(version:String)
    case getHotCate(limit:String, offset:String, time:String)
    case getAllGame(shortName:String)
    case getHotRoom
    case getColumnRoom(limit:Int, offset:Int)
}

extension ChannelApi: TargetType {
    //服务器地址
    public var baseURL: URL {
        switch self {
        case .getbigDataRoom:
            return URL(string: "http://capi.douyucdn.cn/")!
        case .getCycleDta:
            return URL(string: "http://www.douyutv.com/")!
        case .getHotCate,.getAllGame:
            return URL(string: "http://capi.douyucdn.cn/")!
          
        default :
            return URL(string: "http://capi.douyucdn.cn/")!
        }
    }
     
    //各个请求的具体路径
    public var path: String {
        switch self {
        case .getbigDataRoom:
                return "api/v1/getbigDataRoom"
        case .getCycleDta(_):
            return "api/v1/slide/6"
        case .getHotCate:
            return "api/v1/getHotCate"
        case .getAllGame(_):
            return "api/v1/getColumnDetail"
        case .getHotRoom:
            return "api/v1/getHotRoom/2"
        case .getColumnRoom:
            return "api/v1/getColumnRoom/3"
        }
        
    }
    
    //请求类型
    public var method: Moya.Method {
//        switch self {
//        case "":
//            return .post
//        default:
            return .get
//        }
    }
    
    //请求任务事件（这里附带上参数）
    public var task: Task {
        switch self {
        case .getbigDataRoom(let time):
            return .requestParameters(
                 parameters: [
                    "time": time],
                encoding: URLEncoding.default)
            
        case .getCycleDta(let version):
            return .requestParameters(
                 parameters: [
                    "version": version],
                encoding: URLEncoding.default)
        case .getHotCate(let limit, let offset, let time):
            return .requestParameters(
                 parameters: ["limit": limit, "offset" : offset, "time": time],
                encoding: URLEncoding.default)
        case .getAllGame(let shortName):
            return .requestParameters(
                 parameters: [
                    "shortName": shortName],
                encoding: URLEncoding.default)
        case .getColumnRoom(let limit, let offset):
            return .requestParameters(
                 parameters: ["limit": limit, "offset" : offset],
                encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }
     
    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
     
    //请求头
    public var headers: [String: String]? {
         switch self {
         case .getbigDataRoom,.getCycleDta:
             return ["Content-type" : "application/json"]
         default:
             return ["Content-type" : "application/json"]
         }
    }
}
