//
//  RecommendViewController.swift
//  DYZB
//
//  Created by LSL on 2022/1/20.
//

import UIKit

private let kCycleViewH : CGFloat = _gScreenWidth * 3 / 8
private let kGameViewH : CGFloat = 90


class RecommendViewController: BaseAnchorViewController {
    //MARK: - 懒加载属性
    private lazy var recommendVM : RecommendViewModel = RecommendViewModel()
    
    private lazy var cycleView : RecommendCycleView = {
        let cycleView = RecommendCycleView.recommendCycleView()
        cycleView.frame = CGRect(x: 0, y: -kCycleViewH-kGameViewH, width: _gScreenWidth, height: kCycleViewH)
        return cycleView;
    }()
    private lazy var recommendGameView : RecommendGameView = {
        let recommendGameView = RecommendGameView.recommendGameView()
        recommendGameView.frame = CGRect(x: 0, y: -kGameViewH, width: _gScreenWidth, height: kGameViewH)
        return recommendGameView
    }()
   
    override func setupUI() {
        //1.先调用super
        super.setupUI()
        
        //2.将CycleView添加到collcetionView中
        collectionView.addSubview(cycleView)
        
        //3.
        collectionView.addSubview(recommendGameView)
        
        //4.设置collectionView的内边距
        collectionView.contentInset = UIEdgeInsets(top: kCycleViewH+kGameViewH, left: 0, bottom: 0, right: 0)
    }
}

extension RecommendViewController {
    
    
    override func loadData() {
        baseVM = recommendVM
        
        //下面这种block里面不会产出循环引用的问题
        recommendVM.requestData {
            self.collectionView.reloadData()
            
            //2.将数据传递给gameView
            var groups = self.recommendVM.anchorGroups
            //1.移除前二组数据
            if groups.count > 1 {
                groups.removeFirst()
                groups.removeFirst()
            }
            //2.添加更多组
            let dic : [String : Any] = [:]
            let moreGroup = AnchorGroup(dict:dic )
            moreGroup.tag_name = "更多"
            
            groups.append(moreGroup)
            //把数据处理好之后在传给gameView
            self.recommendGameView.groups = groups
            
            self.loadDataFinished()
        }
        
        //获取轮播数据
        recommendVM.requestCycleData {
            self.cycleView.cycleModels = self.recommendVM.cycleModels
        }
        
    }
}

//MARK: - 遵守UICollectionView数据源协议
extension RecommendViewController {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1.定义cell
        var cell : UICollectionViewCell!
        
        //2.取出cell
        if indexPath.section == 1 {
           cell = collectionView.dequeueReusableCell(withReuseIdentifier: kPrettyCellID, for: indexPath) as! CollectionPrettyCell
            
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: kNormalCellID, for: indexPath)
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 1 {
            return CGSize(width: kItemW, height: kPrettyItemH)
        } else {
            return CGSize(width: kItemW, height: kItemH)
        }
    }
}
