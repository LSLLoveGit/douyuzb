//
//  GameViewController.swift
//  DYZB
//
//  Created by LSL on 2022/2/13.
//

import UIKit

private let kEdgeMargin : CGFloat = 10
//fileprivate let kItemW : CGFloat = (_gScreenWidth - 2*kEdgeMargin)/3
//fileprivate let kItemH : CGFloat = kItemW * 6 / 5
fileprivate let kGameCellID = "kGameCellID"
//fileprivate let kHeaderViewH : CGFloat = 50
//fileprivate let kHeaderCellID = "kHeaderCellID"
private let kGameViewH : CGFloat = 90

class GameViewController: BaseViewController {
    
    //MARK: - 懒加载属性
    fileprivate lazy var gameVM : GameViewModel = GameViewModel()
    
    fileprivate lazy var collectionView : UICollectionView = {[unowned self] in
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: kItemW, height: kItemH)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: kEdgeMargin, bottom: 0, right: kEdgeMargin)
        
        layout.headerReferenceSize = CGSize(width: _gScreenWidth, height: kHeaderViewH)
        
        let collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        //注册cell
        collectionView.register(UINib(nibName: "CollectionGameCell", bundle: nil), forCellWithReuseIdentifier: kGameCellID)
        collectionView.register(CollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kHeaderCellID)
        collectionView.dataSource = self
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return collectionView
    }()
    fileprivate lazy var topHeaderView : CollectionHeaderView = {
        let headerView = CollectionHeaderView(frame: CGRect(x: 0, y: -kHeaderViewH-kGameViewH, width: _gScreenWidth, height: kHeaderViewH))
        headerView.leftImgView.image = UIImage(named: "")
        headerView.rightBtn.isHidden = true
        headerView.titleLabel.text = "常见"
        return headerView
    }()
    fileprivate lazy var gameView : RecommendGameView = {
        let gameView = RecommendGameView.recommendGameView()
        gameView.frame = CGRect(x: 0, y: -kGameViewH, width: _gScreenWidth, height: kGameViewH)
        return gameView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        loadData()
    }
    
    override func setupUI() {
        contentView = collectionView
        
        view.addSubview(collectionView)
        
        collectionView.addSubview(topHeaderView)
        
        collectionView.addSubview(gameView)
        
        //4.设置collectionView的内边距
        collectionView.contentInset = UIEdgeInsets(top: kHeaderViewH+kGameViewH, left: 0, bottom: 0, right: 0)
        
        super.setupUI()
    }
    
}

extension GameViewController {
    
    fileprivate func loadData() {
        //下面这种block里面不会产出循环引用的问题
        gameVM.loadAllGameData {
            self.collectionView.reloadData()
            
            //2.展示常用游戏（前10条数据）
            if self.gameVM.gamesArr.count >= 10 {
//                self.gameView.groups = Array(self.gameVM.gamesArr(0..<10))
            }
            
            self.loadDataFinished()
        }
    }
}

extension GameViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.gameVM.gamesArr.count
        return 60
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kGameCellID, for: indexPath) as! CollectionGameCell
        cell.iconImgView.backgroundColor = UIColor.randomColor()
//        cell.game = self.gameVM.gamesArr[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //1,取出section的headerView
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: kHeaderCellID, for: indexPath) as! CollectionHeaderView
        headerView.titleLabel.text = "全部"
        headerView.leftImgView.image = UIImage(named: "")
        headerView.rightBtn.isHidden = true
        return headerView
    }
    
    
    
}
