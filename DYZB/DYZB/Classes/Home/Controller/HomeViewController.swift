//
//  HomeViewController.swift
//  DYZB
//
//  Created by LSL on 2022/1/16.
//

import UIKit

private let kTitleViewH: CGFloat = 40

class HomeViewController: BaseViewController {
    
    //MARK: -懒加载属性
    private lazy var pageTitleView: PageTitleView = {[weak self] in
        let titleFrame = CGRect(x: 0, y: _gStatusBarH+_gNavigationHeight, width: _gScreenWidth, height: kTitleViewH)
        let titles = ["推荐", "游戏", "娱乐", "趣玩"]
        let titleView = PageTitleView(frame: titleFrame, titles: titles)
        titleView.delegate = self
        return titleView
    }()
    private lazy var pageContentView: PageContentView = {[weak self] in
        //1.确定内容的frame
        let contentH = _gScreenHeight-(_gStatusBarH+_gNavigationHeight+kTitleViewH)-_gTabBarMargin-_gTabBarHeight
        let contentFrame = CGRect(x: 0, y: _gStatusBarH+_gNavigationHeight+kTitleViewH, width: _gScreenWidth, height: contentH)
        
        //2.确定所有的子控制器
        var childVcs = [UIViewController]()
        childVcs.append(RecommendViewController())
        childVcs.append(GameViewController())
        childVcs.append(AmuseViewController())
        childVcs.append(FunnyViewController())
        
        let pageContentView = PageContentView(frame: contentFrame, childVcs: childVcs, parentViewController: self)
        pageContentView.delegate = self
        return pageContentView
    }()
    
    //MARK: -系统回调函数
    override func viewDidLoad() {
        super.viewDidLoad()

        //设置UI界面
        setupUI()
        
    }
    
    override func setupUI() {
        //不需要调整UIScrollView的内边距
        //下面语句不加会造成界面滑动到第一个的时候出现错乱
        if #available(iOS 11.0, *) {
//            pageTitleView.scrollView.contentInsetAdjustmentBehavior = .never
        }
       //
        setupNavigationBar()
        
        //2.添加TitleView
        self.view.addSubview(pageTitleView)
        
        //3.添加ContentView
        self.view.addSubview(pageContentView)
        pageContentView.backgroundColor = .purple
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:-设置UI界面
extension HomeViewController {
    
    
    private func setupNavigationBar() {
        //设置左侧的item
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "logo"), for: .normal)
//        btn.sizeToFit()
//
//        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(imageName: "logo")
        
        let size = CGSize(width: 40, height: 40)
        
//        let historyBtn = UIButton()
//        historyBtn.setImage(UIImage(named: "image_my_history"), for: .normal)
//        historyBtn.setImage(UIImage(named: "image_my_history_click"), for: .highlighted)
//        historyBtn.frame = CGRect(origin: CGPoint.zero, size: size)
        let historyItem = UIBarButtonItem(imageName: "image_my_history", hightImageName: "image_my_history_click", size: size)
        
        let searchItem = UIBarButtonItem(imageName: "btn_search", hightImageName: "btn_search_clicked", size: size)
        
        //类方法
//        let qrcodeItem = UIBarButtonItem.createItem(imageName: "Image_scan", hightImageName: "Image_scan_click", size: size)
        //构造方法
        let qrcodeItem = UIBarButtonItem(imageName: "Image_scan", hightImageName: "Image_scan_click", size: size)
        
        navigationItem.rightBarButtonItems = [historyItem,searchItem,qrcodeItem]
    }
}

//MARK: - 遵守协议 PageTitleViewDelegate
extension HomeViewController: PageTitleViewDelegate {
    func pageTitleView(titleView: PageTitleView, selectedIndex index: Int) {
        pageContentView.setupCurrentIndex(currentIndex: index)
    }
}

//MARK: - 遵守协议 PageContentViewDelegate
extension HomeViewController: PageContentViewDelegate {
    func pageContentView(contentView: PageContentView, progress: CGFloat, sourceIndex: Int, targetIndex: Int) {
        pageTitleView.setTitleWithProgress(progress: progress, sourceIndex: sourceIndex, targetIndex: targetIndex)
    }
}
