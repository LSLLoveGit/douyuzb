//
//  AmuseViewController.swift
//  DYZB
//
//  Created by LSL on 2022/2/19.
//

import UIKit

fileprivate let kMenuViewH : CGFloat = 200

class AmuseViewController: BaseAnchorViewController {

    //MARK: - 懒加载属性
    fileprivate lazy var amuseVM : AmuseViewModel = AmuseViewModel()
    fileprivate lazy var menuView : AmuseMenuView = {
        let menuView = AmuseMenuView.amuseMenuView()
        menuView.frame = CGRect(x: 0, y: -kMenuViewH, width: _gScreenWidth, height: kMenuViewH)
        
        return menuView
    }()
    
    override func setupUI() {
        super.setupUI()
        
        //应该加在collectionView上
        collectionView.addSubview(menuView)
        
        collectionView.contentInset = UIEdgeInsets(top: kMenuViewH, left: 0, bottom: 0, right: 0)
    }
}

extension AmuseViewController {
    
}

extension AmuseViewController {
    override func loadData() {
        baseVM = amuseVM
        
        amuseVM.loadAmuseData {
            self.collectionView.reloadData()
            var temGroups = self.amuseVM.anchorGroups
            temGroups.removeFirst()
            self.menuView.groups = temGroups
            
            self.loadDataFinished()
        }
    }
}
