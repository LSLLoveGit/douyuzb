//
//  CycleModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/6.
//

import UIKit
import ObjectMapper

class CycleModel: NSObject {
    var title: String = ""
    var pic_url: String = ""
    var room:[String: Any]? {
        didSet {
            guard let room = room else {
                return
            }
            anchor = AnchorModel(dict: room)
        }
    }
    //主播信息对应的模型对象
    var anchor: AnchorModel?

    init(dict: [String: Any]) {
        super.init()

        setValuesForKeys(dict)
        self.title = dict["title"] as! String
        self.pic_url = dict["pic_url"] as! String
        
        anchor = AnchorModel(dict: dict["room"] as! [String : Any])
    }
    override func setValue(_ value: Any?, forUndefinedKey key: String) {

    }
    
//    var title: String?
//    var pic_url: String?
//    //主播信息对应的模型对象
//    var anchor: AnchorModel?
//
//    required init?(map: Map) {
//
//    }
//
//    // Mappable
//    func mapping(map: Map) {
//        title    <- map["title"]
//        pic_url    <- map["pic_url"]
//        anchor    <- map["room"]
//
//    }
}
