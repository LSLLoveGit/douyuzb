//
//  CollectionCycleCell.swift
//  DYZB
//
//  Created by LSL on 2022/2/6.
//

import UIKit
import Kingfisher
class CollectionCycleCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        autoresizingMask = []
        
        iconImageView.contentMode = .scaleAspectFill
    }

    var cycleModel: CycleModel? {
        didSet {
            titleLabel.text = cycleModel?.title
            let iconUrl = NSURL(string: cycleModel?.pic_url ?? "")!
            //print("----\(iconUrl)")
            let imageResource = ImageResource(downloadURL: iconUrl as URL, cacheKey: nil)
            iconImageView.kf.setImage(with: imageResource, placeholder: nil, options: nil, completionHandler: nil)
        }
    }
}
