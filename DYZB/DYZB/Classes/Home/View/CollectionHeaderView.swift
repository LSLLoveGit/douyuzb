//
//  CollectionHeaderView.swift
//  DYZB
//
//  Created by LSL on 2022/1/22.
//

import UIKit

class CollectionHeaderView: UICollectionReusableView {
    let leftImgView : UIImageView = UIImageView()
    let titleLabel = UILabel()
    let rightBtn = UIButton()
    
    var anchor: AnchorGroup? {
        didSet {
            guard let anchor = anchor else {
                return
            }
            titleLabel.text = anchor.tag_name
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    } 
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        
    }
    
    func setupUI() {
        self.backgroundColor = .white
        
        let lineView = UIView()
        lineView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 10)
        lineView.backgroundColor = UIColor.init(r: 234, g: 234, b: 234, alpha: 1.0)
        self.addSubview(lineView)
        
        leftImgView.frame = CGRect(x: 10, y: 21, width: 18, height: 18)
        leftImgView.contentMode = .scaleAspectFit
        self.addSubview(leftImgView)
        
        titleLabel.frame = CGRect(x: 10+18+5, y: leftImgView.frame.origin.y, width: 100, height: 18)
        titleLabel.textColor = .gray
        titleLabel.textAlignment = .left
        titleLabel.text = "颜值"
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(titleLabel)
        
        rightBtn.frame = CGRect(x: self.frame.size.width-10-60, y: 10+5, width: 60, height: 30)
        self.addSubview(rightBtn)
        rightBtn.setTitle("更多>", for: .normal)
        rightBtn.setTitleColor(.darkText, for: .normal)
        rightBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        rightBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        
    }
}
