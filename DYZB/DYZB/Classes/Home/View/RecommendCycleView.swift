//
//  RecommendCycleView.swift
//  DYZB
//
//  Created by LSL on 2022/2/5.
//

import UIKit

private let kCycleCellID = "kCycleCellID"
private let kCycleViewH : CGFloat = _gScreenWidth * 3 / 8

class RecommendCycleView: UIView {
    var cycleTimer : Timer?
    var cycleModels : [CycleModel]? {
        didSet {
            collectionView.reloadData()
            
            pageControl.numberOfPages = cycleModels?.count ?? 0
            
            //3.默认滚动到中间某个位置
            let indexPath = NSIndexPath.init(item: (cycleModels?.count ?? 0) * 10, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: .left, animated: false)
            
            //添加定时器
            removeCycleTimer()
            addCycleTimer()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //设置该控件不随着父控件的拉伸而拉伸
        autoresizingMask = []
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        //注册cell
        collectionView.register(UINib(nibName: "CollectionCycleCell", bundle: nil), forCellWithReuseIdentifier: kCycleCellID)
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = collectionView.bounds.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal//水平滚动
        collectionView.isPagingEnabled = true
    }
}

// 提供快速创建View的类方法
extension RecommendCycleView {
    class func recommendCycleView() -> RecommendCycleView {
        return Bundle.main.loadNibNamed("RecommendCycleView", owner: self, options: nil)?.first as! RecommendCycleView
    }
}

// 遵守UICollectionView的数据源方法
extension RecommendCycleView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (cycleModels?.count ?? 0) * 10000
//        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCycleCellID, for: indexPath) as! CollectionCycleCell
        //下面使用！解包因为上面的numberOfItemsInSection已经判断为0就不走这个方法，所以不用担心报错
        cell.cycleModel = cycleModels![indexPath.item % cycleModels!.count]

        return cell
    }
    
    
}

extension RecommendCycleView : UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetX = scrollView.contentOffset.x + scrollView.bounds.width / 2
        
        pageControl.currentPage = Int(offsetX / scrollView.bounds.width) % (cycleModels?.count ?? 1)
    }
    //拖动view先停掉定时器
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeCycleTimer()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        addCycleTimer()
    }
}

// 对定时器的操作方法
extension RecommendCycleView {
    private func addCycleTimer() {
        cycleTimer = Timer(timeInterval: 3.0, repeats: true, block: {[weak self] timer in
            let currentOffsetX = self?.collectionView.contentOffset.x
            let offsetX = currentOffsetX! + CGFloat((self?.collectionView.bounds.width)!)
            self?.collectionView.setContentOffset(CGPoint(x: offsetX, y: 0), animated: true)
        })
        //把定时器加入到运行循环里面，并且加到mainRunloop
        RunLoop.main.add(cycleTimer!, forMode: .common)
    }
    
    private func removeCycleTimer() {
        cycleTimer?.invalidate()//从运行循环中移除
        cycleTimer = nil
    }
}
