//
//  CollectionGameCell.swift
//  DYZB
//
//  Created by LSL on 2022/2/10.
//

import UIKit
import SwiftUI
import Kingfisher

class CollectionGameCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    //定义模型属性
    var group : BaseGameModel? {
        didSet {
            guard let group = group else {return}
            titleLab.text = group.tag_name
            guard let iconUrl = NSURL(string: group.icon_url) else {return}
            //print("----\(iconUrl)")
            let imageResource = ImageResource(downloadURL: iconUrl as URL, cacheKey: nil)
            iconImgView.kf.setImage(with: imageResource, placeholder: UIImage(named: "home_more_btn"), options: nil, completionHandler: nil)

        }
    }
    
    var game : GameModel? {
        didSet {
            guard let group = group else {return}
            titleLab.text = group.tag_name
            guard let iconUrl = NSURL(string: group.icon_url) else {return}
            //print("----\(iconUrl)")
            let imageResource = ImageResource(downloadURL: iconUrl as URL, cacheKey: nil)
            iconImgView.kf.setImage(with: imageResource, placeholder: UIImage(named: "home_more_btn"), options: nil, completionHandler: nil)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
