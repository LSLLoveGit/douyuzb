//
//  RecommendViewModel.swift
//  DYZB
//
//  Created by LSL on 2022/1/23.
//

import UIKit
import Alamofire

class RecommendViewModel: BaseViewModel {
    
    lazy var cycleModels : [CycleModel] = [CycleModel]()
//    private lazy var bigDataGroup : AnchorGroup = AnchorGroup()
//    private lazy var prettyGroup : AnchorGroup = AnchorGroup()
    
}

//MARK: -  发送网络请求
extension RecommendViewModel {
    func requestData(finishCallBack: @escaping () -> ()) {

        let dicts: [String: String] = ["limit": "4", "offset" : "0", "time": Date.getCurrentTime()]

        //1.创建Group
        let dGroup = DispatchGroup()
      
        
        //请求第一部分推荐数据
        dGroup.enter()
        NetworkTools.request(.getbigDataRoom(time: Date.getCurrentTime())) { result in
            //print(result)
            
            dGroup.leave()
        } error: { statusCode, errorInfo in
            print(errorInfo)
            
            dGroup.leave()
        } failure: { error in
            print(error)
            
            dGroup.leave()
        }

       
        // 4.请求第二部分颜值数据
        dGroup.enter()
        
        let b = AF.request("http://capi.douyucdn.cn/api/v1/getVerticalRoom", method: .get, parameters:dicts,encoder: JSONParameterEncoder.default,headers: ["Accept":"application/json"])
        b.response{
            response in
//            if let data = response.data {
//
//            }
            
            dGroup.leave()
        }
        
        // 5.请求2-12部分的游戏数据
        dGroup.enter()
        
//        NetworkTools.request(.getHotCate(limit: "4", offset: "0", time: Date.getCurrentTime())) {[weak self] result in
//            let resultDict : Dictionary = result.dictionary!
//            //2.根据data的key获取数据
//            let dataArray = resultDict["data"]?.rawValue as! [[String : Any]]
//
//            //3.字典转模型
//            for dict in dataArray {
//                let group = AnchorGroup(dict: dict)
//                self?.anchorGroups.append(group)
//            }
//
//            dGroup.leave()
//        } error: { statusCode, errorInfo in
////            print(errorInfo)
//            dGroup.leave()
//        } failure: { error in
//            print(error)
//            dGroup.leave()
//        }
        loadAnchorData(true,.getHotCate(limit: "4", offset: "0", time: Date.getCurrentTime())) {
            dGroup.leave()
        }
        
        //6.所有的数据都请求到之后进行排序
        dGroup.notify(queue: DispatchQueue.main) {
//            self.anchorGroups.insert(self.prettyGroup, at: 0)
//            self.anchorGroups.insert(self.bigDataGroup, at: 0)
            finishCallBack()
        }
    }
    
    //请求无限轮播的数据
    func requestCycleData(finishCallBack: @escaping () -> ()) {
        NetworkTools.request(.getCycleDta(version: "2.300")) {[weak self] result in
            let resultDict : Dictionary = result.dictionary!
            //2.根据data的key获取数据
            let dataArray = resultDict["data"]?.rawValue as! [[String : Any]]

            //3.字典转模型
            for dict in dataArray {
                self?.cycleModels.append(CycleModel(dict: dict))
            }

            finishCallBack()
        } error: { statusCode, errorInfo in
            print(errorInfo)

        } failure: { error in
            print(error)

        }
    }
}
