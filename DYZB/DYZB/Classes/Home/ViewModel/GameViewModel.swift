//
//  GameViewModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/14.
//

import UIKit

class GameViewModel {
    lazy var gamesArr : [GameModel] = [GameModel]()
}

extension GameViewModel {
    //请求无限轮播的数据
    func loadAllGameData(_ finishCallBack: @escaping () -> ()) {
        NetworkTools.request(.getHotCate(limit: "4", offset: "0", time: Date.getCurrentTime())) {[weak self] result in
            guard let resultDict : Dictionary = result.dictionary else {return}
            //2.根据data的key获取数据
            guard let dataArray = resultDict["data"]?.rawValue as? [[String : Any]] else {return}
            
            //3.字典转模型
            for dict in dataArray {
                self?.gamesArr.append(GameModel(dict: dict))
            }
            finishCallBack()
        } error: { statusCode, errorInfo in
//            print(errorInfo)
            
        } failure: { error in
            print(error)
            
        }
    }
}
