//
//  AmuseViewModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/19.
//

import UIKit

class AmuseViewModel : BaseViewModel{
    
}

extension AmuseViewModel {
    //请求无限轮播的数据
    func loadAmuseData(_ finishCallBack: @escaping () -> ()) {
//        NetworkTools.request(.getHotRoom) {[weak self] result in
//            guard let resultDict : Dictionary = result.dictionary else {return}
//            print(resultDict)
//            //2.根据data的key获取数据
//            guard let dataArray = resultDict["data"]?.rawValue as? [[String : Any]] else {return}
//
//            //3.字典转模型
//            for dict in dataArray {
//                let roomListArray = dict["room_list"] as? [[String : Any]]
//                guard let roomListArray = roomListArray else {
//                    return
//                }
//                let anchorGroup = AnchorGroup(dict: dict)
//                for dict in roomListArray {
//                    anchorGroup.anchors.append(AnchorModel(dict: dict))
//                }
//                self?.anchorGroups.append(anchorGroup)
//            }
//            finishCallBack()
//        } error: { statusCode, errorInfo in
//            print(errorInfo)
//
//        } failure: { error in
//            print(error)
//
//        }
        loadAnchorData(true,.getHotRoom, finishCallBack)
    }
}
