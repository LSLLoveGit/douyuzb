//
//  FunnyViewModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/20.
//

import UIKit

class FunnyViewModel: BaseViewModel {

}

extension FunnyViewModel {
    func loadFunnyData(_ finishedCallback: @escaping () -> ()) {
        loadAnchorData(false,.getColumnRoom(limit: 30, offset: 0), finishedCallback)
    }
}
