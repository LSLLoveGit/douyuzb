//
//  CollectionNormalCell.swift
//  DYZB
//
//  Created by LSL on 2022/1/23.
//

import UIKit
import Kingfisher

class CollectionNormalCell: UICollectionViewCell {
    let topImgView = UIImageView()
    let leftSmallImgView = UIImageView()
    let titleLabel = UILabel()
    let nameLabel = UILabel()
    let numBtn = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    convenience init() {
//        self.init(frame: CGRect.zero)
//    }
    //定义模型属性
    var anchor : AnchorModel? {
        didSet {
            guard let anchor = anchor else {return}
            titleLabel.text = anchor.room_name
            guard let iconUrl = NSURL(string: anchor.vertical_src) else {return}
            //print("----\(iconUrl)")
            let imageResource = ImageResource(downloadURL: iconUrl as URL, cacheKey: nil)
            topImgView.kf.setImage(with: imageResource, placeholder: UIImage(named: "home_more_btn"), options: nil, completionHandler: nil)
            
            nameLabel.text = anchor.nickname
            numBtn.setTitle(String(anchor.online), for: .normal)
        }
    }
    
    func setupUI() {
        self.backgroundColor = .white
        
        topImgView.frame = CGRect(x: 10, y: 21, width: 18, height: 18)
        topImgView.contentMode = .scaleAspectFill
        topImgView.backgroundColor = .lightGray
        self.addSubview(topImgView)
        topImgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-35)
        }
        topImgView.layer.cornerRadius = 5
        topImgView.layer.masksToBounds = true
        
        leftSmallImgView.contentMode = .scaleAspectFit
        self.addSubview(leftSmallImgView)
        leftSmallImgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0)
            make.size.equalTo(CGSize(width: 14, height: 14))
            make.top.equalTo(topImgView.snp.bottom).offset(5)
            make.bottom.equalToSuperview().offset(-10)
        }
        
        titleLabel.textColor = .darkText
        titleLabel.textAlignment = .left
        titleLabel.text = "Swift学习"
        titleLabel.font = UIFont.systemFont(ofSize: 11)
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(leftSmallImgView.snp.right).offset(5)
            make.right.equalToSuperview().offset(-5)
            make.centerY.equalTo(leftSmallImgView.snp.centerY)
            
        }
        
        nameLabel.textColor = .white
        nameLabel.textAlignment = .left
        nameLabel.text = "主播小哥哥"
        nameLabel.font = UIFont.systemFont(ofSize: 11)
        self.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(topImgView).offset(5)
            make.size.equalTo(CGSize(width: 150, height: 20))
            make.bottom.equalTo(topImgView.snp.bottom).offset(-5)
        }
        
        self.addSubview(numBtn)
        numBtn.setImage(UIImage(named: ""), for: .normal)
        numBtn.setTitle("6666在线", for: .normal)
        numBtn.setTitleColor(.darkText, for: .normal)
        numBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        numBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        numBtn.snp.makeConstraints { (make) in
            make.right.equalTo(topImgView).offset(-5)
            make.size.equalTo(CGSize(width: 150, height: 20))
            make.bottom.equalTo(topImgView.snp.bottom).offset(-5)
        }
        
        
    }
}
