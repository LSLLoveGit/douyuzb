//
//  BaseViewController.swift
//  DYZB
//
//  Created by LSL on 2022/1/16.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {
    var hidenNavigationBar = false
    var contentView : UIView?
    
    fileprivate lazy var animImageView : UIImageView = {[unowned self] in
        let imageView = UIImageView(image: UIImage(named: "Icon-180"))
        imageView.center = self.view.center
        imageView.animationImages = [UIImage(named: "Icon-180")!,UIImage(named: "Icon-180")!]
        imageView.animationDuration = 0.5
        imageView.animationRepeatCount = LONG_MAX
        //随着父控制器的顶部和底部拉伸而拉伸，这样永远是居中显示了
        imageView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin]
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //设置导航栏背景为透明色图片
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //设置导航栏阴影为透明色图片
        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "返回", image: UIImage(), primaryAction: nil, menu: nil)
        navigationItem.backBarButtonItem?.tintColor = .black
        
        setupUI()
    }
    
    open func resetNavigationBar() {
        navigationController?.navigationBar.isHidden = hidenNavigationBar
    }
    
    // 添加自定义导航栏背景
    func addNavBar(_ color: UIColor) {
        // 目前有点小瑕疵，高度是写死的，并没有考虑到 SafeArea
        // 我会在之后的一篇中分析 SafeArea 时，给出如何正确的适配不同机型
        let size = CGSize(width: view.bounds.width, height: Double(_gStatusBarH+_gNavigationHeight))
//        let navImageView = UIImageView(image: UIImage(size: size, color: color))
        let image : UIImage! = UIImage()
        
        let navImageView = UIImageView(image: image)
        navImageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        navImageView.backgroundColor = color
        view.addSubview(navImageView)
    }
    
    func setupUI() {
        contentView?.isHidden = true
        
        view.addSubview(animImageView)
        
        animImageView.startAnimating()
        
        view.backgroundColor = UIColor(r: 250, g: 250, b: 250, alpha: 1)
    }
   
    func loadDataFinished() {
        animImageView.stopAnimating()
        animImageView.isHidden = true
        contentView?.isHidden = false
    }

}

extension BaseViewController {
    
}
