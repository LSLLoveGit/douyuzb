//
//  CustomNavigationController.swift
//  DYZB
//
//  Created by LSL on 2022/1/16.
//

import UIKit

class CustomNavigationController: UINavigationController,UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        //1.获取系统的Pop手势
        guard let systemGes = interactivePopGestureRecognizer else {return}
        //2.获取手势添加到view上
        guard let gesView = systemGes.view else {return}
        
        //3.获取target/action
        //3.1利用运行时机制查看所有的属性名称
        /*
        var count : UInt32 = 0
        let ivars = class_copyIvarList(UIGestureRecognizer.self, &count)!
        for i in 0..<count {
            let ivar = ivars[Int(i)]
            guard let name = ivar_getName(ivar) else { return }
            print(String(cString: name))
        }
         */
        let targets = systemGes.value(forKey: "_targets") as? [NSObject]
        guard let targetObjc = targets?.first else {return}
        
        guard let target = targetObjc.value(forKey: "target") else {return}
        let action = Selector(("handleNavigationTransition:"))
        
        //创建自己的手势
        let panGes = UIPanGestureRecognizer()
        gesView.addGestureRecognizer(panGes)
        panGes.addTarget(target, action: action)
    }
    // 重写 pushViewController 方法，不修改 pushViewController 的逻辑
    // 仅在跳转前，判断目标 VC 是否为一级页面还是二级页面，通过 viewControllers.count 来判断：
    // viewControllers.count > 0，那么目标 VC 肯定是第二个页面（即二级页面）；我们就添加上 hidesBottomBarWhenPushed = true
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    //重写 navigationController:didShow:animated,在完全退出时，强制类型转为 BaseViewController
    //并重置 导航栏隐藏与显示
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        var vc: BaseViewController?
        if viewControllers.count > 0 {
            vc = viewControllers.last as? BaseViewController
            vc!.resetNavigationBar()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
