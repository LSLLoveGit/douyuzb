//
//  BaseAnchorViewController.swift
//  DYZB
//
//  Created by LSL on 2022/2/19.
//

import UIKit

 let kItemMargin: CGFloat = 10
 let kItemW : CGFloat = (_gScreenWidth - 3*kItemMargin) / 2
 let kItemH : CGFloat = kItemW * 3 / 4
 let kPrettyItemH : CGFloat = kItemW * 4 / 3
 let kHeaderViewH : CGFloat = 50

 let kNormalCellID = "kNormalCellID"
 let kPrettyCellID = "kPrettyCellID"
 let kHeaderCellID = "kHeaderCellID"

class BaseAnchorViewController: BaseViewController {

    var baseVM : BaseViewModel!
    
    lazy var collectionView: UICollectionView = {[unowned self] in
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: kItemW, height: kItemH)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = kItemMargin
        layout.headerReferenceSize = CGSize(width: _gScreenWidth, height: kHeaderViewH)
        layout.sectionInset = UIEdgeInsets(top: 0, left: kItemMargin, bottom: 0, right: kItemMargin)
        
        let collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        //设置collectionView的宽高随着父控制器的改变而改变
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: kNormalCellID)
        //collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kHeaderCellID)
        
        collectionView.register(CollectionNormalCell.self, forCellWithReuseIdentifier: kNormalCellID)
        collectionView.register(UINib(nibName: "CollectionPrettyCell", bundle: nil), forCellWithReuseIdentifier: kPrettyCellID)
        collectionView.register(CollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kHeaderCellID)
        
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //设置UI界面
        setupUI()
        
        loadData()
    }
    
    override func setupUI() {
        contentView = collectionView
        
        //将UICollectionView 添加控制器view里面
        self.view.addSubview(collectionView)
        
        super.setupUI()
    }
}

extension BaseAnchorViewController {
    
    
}

extension BaseAnchorViewController {
    @objc func loadData() {
        
    }
}

//MARK: - 遵守UICollectionView数据源协议
extension BaseAnchorViewController : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return baseVM.anchorGroups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return baseVM.anchorGroups[section].anchors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1.定义cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kNormalCellID, for: indexPath) as! CollectionNormalCell
        cell.anchor = baseVM.anchorGroups[indexPath.section].anchors[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //1,取出section的headerView
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: kHeaderCellID, for: indexPath) as! CollectionHeaderView
        headerView.anchor = baseVM.anchorGroups[indexPath.section]
        return headerView
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: kItemW, height: kItemH)
    }
}

extension BaseAnchorViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let anchor = baseVM.anchorGroups[indexPath.section].anchors[indexPath.item]
        //判断是秀场房间还是普通房间 //0:电脑直播(普通房间) 1：手机直播(秀场房间)
        anchor.isVertical == 0 ? pushNormalRoomVc() : presentShowRoomVc()
    }
    
    private func presentShowRoomVc() {
        let showRoomVc = RoomShowViewController()
        present(showRoomVc, animated: true, completion: nil)
    }
    
    private func pushNormalRoomVc() {
        let normalRoomVc = RoomNormalViewController()
        navigationController?.pushViewController(normalRoomVc, animated: true)
    }
}
