//
//  AnchorModel.swift
//  DYZB
//
//  Created by LSL on 2022/1/25.
//

import UIKit

class AnchorModel: NSObject {
    var room_name: String = ""
    var nickname: String = ""
    var game_name: String = ""
    var vertical_src: String = ""//房间图片展位图
    var avatar_mid: String = ""//个人占位图
    var avatar_small: String = ""
    var online: Int = 0
    //0:电脑直播(普通房间) 1：手机直播(秀场房间)
    var isVertical : Int = 0
    init(dict: [String: Any]) {
        super.init()
        
        setValuesForKeys(dict)
        
        self.room_name = dict["room_name"] as? String ?? ""
        self.nickname = dict["nickname"] as? String ?? ""
        self.game_name = dict["game_name"] as? String ?? ""
        self.vertical_src = dict["vertical_src"] as? String ?? ""
        self.avatar_mid = dict["avatar_mid"] as? String ?? ""
        self.avatar_small = dict["avatar_small"] as? String ?? ""
        self.online = dict["online"] as? Int ?? 0
        self.isVertical = dict["isVertical"] as? Int ?? 0
    }
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        
    }
    
    
}
