//
//  AnchorGroup.swift
//  DYZB
//
//  Created by LSL on 2022/1/25.
//

import UIKit

class AnchorGroup: BaseGameModel {
    // 该组中对应的房间信息
    var room_list : [[String : Any]]? {
        didSet {
            guard let room_list = room_list else {
                return
            }
            for dict in room_list {
                anchors.append(AnchorModel(dict: dict))
            }
        }
    }
    
    
    //组显示的图标
    var icon_name : String = "home_header_normal"
    
    
    lazy var anchors : [AnchorModel] = [AnchorModel]()
    
//    override init(dict: [String : Any]) {
//        super.init()
//        setValuesForKeys(dict)
//
//        self.icon_name = dict["icon_name"] as? String ?? ""
//
//        self.room_list = dict["room_list"] as? [[String : Any]]
//        guard let room_list = room_list else {
//            return
//        }
//        for dict in room_list {
//            anchors.append(AnchorModel(dict: dict))
//        }
//
//    }
   
}
