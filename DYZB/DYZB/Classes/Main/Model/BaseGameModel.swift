//
//  BaseGameModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/14.
//

import UIKit

class BaseGameModel:NSObject {
    var tag_name: String = ""
    var icon_url: String = ""

    override init() {
        
    }
    
    init(dict: [String: Any]) {
        super.init()

        setValuesForKeys(dict)
        self.tag_name = dict["tag_name"] as? String ?? ""
        self.icon_url = dict["icon_url"] as? String ?? ""
        
    }
    override func setValue(_ value: Any?, forUndefinedKey key: String) {

    }
}
