//
//  BaseViewModel.swift
//  DYZB
//
//  Created by LSL on 2022/2/19.
//

import UIKit

class BaseViewModel {
    lazy var anchorGroups : [AnchorGroup] = [AnchorGroup]()
}

extension BaseViewModel {
    func loadAnchorData(_ isGroupData: Bool,_ target: ChannelApi,_ finishCallBack: @escaping () -> ()) {
        NetworkTools.request(target) {[weak self] result in
            guard let resultDict : Dictionary = result.dictionary else {return}
            print(resultDict)
            //2.根据data的key获取数据
            guard let dataArray = resultDict["data"]?.rawValue as? [[String : Any]] else {return}
            
            if isGroupData {
                //3.字典转模型
                for dict in dataArray {
                    let roomListArray = dict["room_list"] as? [[String : Any]]
                    guard let roomListArray = roomListArray else {
                        return
                    }
                    let anchorGroup = AnchorGroup(dict: dict)
                    for dict in roomListArray {
                        anchorGroup.anchors.append(AnchorModel(dict: dict))
                    }
                    self?.anchorGroups.append(anchorGroup)
                }
            } else {
                let anchorGroup = AnchorGroup()
                for dict in dataArray {
                    anchorGroup.anchors.append(AnchorModel(dict: dict))
                }
                
                self?.anchorGroups.append(anchorGroup)
            }
            
            finishCallBack()
        } error: { statusCode, errorInfo in
            print(errorInfo)
            
        } failure: { error in
            print(error)
            
        }
    }
}
